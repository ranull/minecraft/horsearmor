package com.ranull.horsearmor.enchants;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import java.util.Random;

public class EnchantManager {
    public Boolean unbreaking(Integer level) {
        Random random = new Random();
        if (level.equals(1)) {
            return random.nextFloat() <= 0.20f;
        } else if (level.equals(1)) {
            return random.nextFloat() <= 0.27f;
        } else if (level.equals(3)) {
            return random.nextFloat() <= 0.30f;
        }
        return null;
    }

    public void frostWalker(Horse horse, int level) {
        if (!horse.getLocation().getBlock().getType().equals(Material.AIR)) {
            return;
        }
        Location loc = horse.getLocation().subtract(0, 1, 0);
        int radius = 2 + level;
        Block middle = loc.getBlock();

        for (int x = radius; x >= -radius; x--) {
            for (int z = radius; z >= -radius; z--) {
                if (middle.getRelative(x, 0, z).getType() == Material.WATER) {
                    middle.getRelative(x, 0, z).setType(Material.FROSTED_ICE);
                }
            }
        }
    }

    public void thorns(Horse horse, Entity attacker, int level) {
        Random random = new Random();
        float randomChance = new Random().nextFloat();
        float chance = 0.15F * (float) level;
        if (chance >= randomChance) {
            if (attacker instanceof LivingEntity) {
                int damage = random.nextInt((4 - 1) + 1) + 1;
                ((LivingEntity) attacker).damage(damage);
                horse.getWorld().playSound(horse.getLocation(), Sound.ENCHANT_THORNS_HIT, 1, 1);
            }
        }
    }

    public void protection(Horse horse, double damage, int level) {
        float protection = 2 * level;
        damage = damage / protection;
        horse.damage(damage);
    }

    public void fireProtection(Horse horse, DamageCause cause, double damage, int level) {
        double damageReduce = damage / (2 * level);
        damage = damage - damageReduce;

        int burnTime = 150;
        if (cause.equals(DamageCause.LAVA)) {
            burnTime = 300;
        }

        horse.damage(damage);
        horse.setFireTicks(burnTime);
    }
}
