package com.ranull.horsearmor.durability;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Horse;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class DurabilityManager {

    @SuppressWarnings("deprecation")
    public Short getDurability(ItemStack armor) {
        return armor.getDurability();
    }

    @SuppressWarnings("deprecation")
    public void setDurability(ItemStack armor, Short dur) {
        if (armor != null) {
            armor.setDurability(dur);
        }
    }

    public Short getMaterialDurability(ItemStack armor) {
        if (armor.getType().equals(Material.IRON_HORSE_ARMOR)) {
            return 240;
        } else if (armor.getType().equals(Material.IRON_HORSE_ARMOR)) {
            return 112;
        } else if (armor.getType().equals(Material.DIAMOND_HORSE_ARMOR)) {
            return 528;
        } else if (armor.getType().equals(Material.LEATHER_HORSE_ARMOR)) {
            return 80;
        }
        return null;
    }

    public Short getRepairAmount(ItemStack armor) {
        if (armor.getType().equals(Material.IRON_HORSE_ARMOR)) {
            return 80;
        } else if (armor.getType().equals(Material.IRON_HORSE_ARMOR)) {
            return 38;
        } else if (armor.getType().equals(Material.DIAMOND_HORSE_ARMOR)) {
            return 176;
        } else if (armor.getType().equals(Material.LEATHER_HORSE_ARMOR)) {
            return 26;
        }
        return null;
    }

    public void changeDurability(ItemStack armor, Short dur) {
        Short currentDur = getDurability(armor);
        if (currentDur.equals((short) 0)) {
            setDefaultDurability(armor);
        }
        Short materialDur = getMaterialDurability(armor);
        currentDur = getDurability(armor);
        Short newDir = (short) (currentDur + dur);
        if (newDir >= materialDur) {
            newDir = materialDur;
        }

        setDurability(armor, newDir);
        changeDurabilityLore(armor, newDir);
    }

    public void changeDurability(Horse horse, ItemStack armor, Short dur) {
        changeDurability(armor, dur);
        Short currentDur = getDurability(armor);
        Short newDir = (short) (currentDur + dur);
        if (newDir <= 0) {
            breakArmor(horse);
            return;
        }
    }

    public void changeDurabilityLore(ItemStack armor, Short dur) {
        ItemMeta meta = armor.getItemMeta();
        Short materialDur = getMaterialDurability(armor);
        String loreString = ChatColor.RESET + "Durability: " + dur + " / " + materialDur;
        if (meta.getLore() == null) {
            List<String> lore = new ArrayList<String>();
            lore.add(loreString);
            meta.setLore(lore);
            armor.setItemMeta(meta);
        } else {
            for (String lore : meta.getLore()) {
                if (lore.startsWith(ChatColor.RESET + "Durability: ")) {
                    List<String> itemLore = meta.getLore();
                    itemLore.remove(lore);
                    itemLore.add(loreString);
                    meta.setLore(itemLore);
                    armor.setItemMeta(meta);
                    return;
                }
            }
            meta.getLore().add(loreString);
            armor.setItemMeta(meta);
        }
    }

    public Material getRepairItem(ItemStack armor) {
        if (armor.getType().equals(Material.IRON_HORSE_ARMOR)) {
            return Material.IRON_INGOT;
        }
        if (armor.getType().equals(Material.GOLDEN_HORSE_ARMOR)) {
            return Material.GOLD_INGOT;
        }
        if (armor.getType().equals(Material.DIAMOND_HORSE_ARMOR)) {
            return Material.DIAMOND;
        }
        if (armor.getType().equals(Material.LEATHER_HORSE_ARMOR)) {
            return Material.LEATHER;
        }
        return null;
    }

    public void breakArmor(Horse horse) {
        horse.getInventory().setArmor(null);
        horse.getWorld().playSound(horse.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
    }

    public void setDefaultDurability(ItemStack armor) {
        Short materialDur = getMaterialDurability(armor);
        setDurability(armor, materialDur);
        changeDurabilityLore(armor, materialDur);
    }
}
