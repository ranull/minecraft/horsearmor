package com.ranull.horsearmor.commands;

import com.ranull.horsearmor.HorseArmor;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class HorseArmorCommand implements CommandExecutor {
    private HorseArmor plugin;

    public HorseArmorCommand(HorseArmor plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.4";
        String author = "Ranull";

        if (args.length < 1) {
            sender.sendMessage(
                    ChatColor.DARK_GRAY + "» " + ChatColor.AQUA + "HorseArmor " + ChatColor.GRAY + "v" + version);
            sender.sendMessage(
                    ChatColor.GRAY + "/horsearmor " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
            if (sender.hasPermission("horsearmor.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/horsearmor reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("horsearmor.reload")) {
                sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "HorseArmor" + ChatColor.DARK_GRAY + "]"
                        + ChatColor.RESET + " No Permission!");
                return true;
            }
            plugin.reloadConfig();
            sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "HorseArmor" + ChatColor.DARK_GRAY + "]"
                    + ChatColor.RESET + " Reloaded config file!");
        }
        return true;
    }
}
