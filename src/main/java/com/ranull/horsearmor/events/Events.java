package com.ranull.horsearmor.events;

import com.ranull.horsearmor.HorseArmor;
import com.ranull.horsearmor.durability.DurabilityManager;
import com.ranull.horsearmor.enchants.EnchantManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.List;
import java.util.Map;

public class Events implements Listener {
    private HorseArmor plugin;
    private EnchantManager enchant;
    private DurabilityManager dur;

    public Events(HorseArmor plugin, EnchantManager enchant, DurabilityManager dur) {
        this.plugin = plugin;
        this.enchant = enchant;
        this.dur = dur;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getVehicle() instanceof Horse) {
            if (!player.hasPermission("horsearmor.frostwalker")) {
                return;
            }
            Horse horse = (Horse) player.getVehicle();
            ItemStack armor = horse.getInventory().getArmor();
            if (armor == null) {
                return;
            }
            List<String> armorList = plugin.getConfig().getStringList("settings.armor");
            if (!armorList.contains(armor.getType().toString())) {
                return;
            }

            if (armor.containsEnchantment(Enchantment.FROST_WALKER)) {
                if (!plugin.getConfig().getBoolean("settings.enchants.frostWalker")) {
                    return;
                }
                int level = armor.getEnchantmentLevel(Enchantment.FROST_WALKER);
                enchant.frostWalker(horse, level);
            }
        }
    }

    @EventHandler
    public void onHorseDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Horse) {
            Horse horse = (Horse) event.getEntity();
            ItemStack armor = horse.getInventory().getArmor();

            if (armor == null) {
                return;
            }
            List<String> armorList = plugin.getConfig().getStringList("settings.armor");
            if (!armorList.contains(armor.getType().toString())) {
                return;
            }

            Boolean damage = true;
            if (armor.containsEnchantment(Enchantment.DURABILITY)) {
                int level = armor.getEnchantmentLevel(Enchantment.DURABILITY);
                if (enchant.unbreaking(level)) {
                    damage = false;
                }
            }

            if (damage) {
                dur.changeDurability(horse, armor, (short) -1);
            }

            if (event.getCause().equals(DamageCause.FIRE) || event.getCause().equals(DamageCause.LAVA)) {
                if (armor.containsEnchantment(Enchantment.PROTECTION_FIRE)) {
                    if (!plugin.getConfig().getBoolean("settings.enchants.fireProtection")) {
                        return;
                    }
                    int level = armor.getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
                    enchant.fireProtection(horse, event.getCause(), event.getDamage(), level);
                    event.setDamage(0);
                }
            }
        }
    }

    @EventHandler
    public void onDamageHorseEntity(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Horse) {
            Horse horse = (Horse) event.getEntity();
            ItemStack armor = horse.getInventory().getArmor();
            if (armor == null) {
                return;
            }
            List<String> armorList = plugin.getConfig().getStringList("settings.armor");
            if (!armorList.contains(armor.getType().toString())) {
                return;
            }

            if (event.getCause().equals(DamageCause.ENTITY_ATTACK)) {
                if (armor.containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL)) {
                    if (!plugin.getConfig().getBoolean("settings.enchants.protection")) {
                        return;
                    }
                    int level = armor.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
                    enchant.protection(horse, event.getDamage(), level);
                    event.setDamage(0);
                }

                if (armor.containsEnchantment(Enchantment.THORNS)) {
                    if (!plugin.getConfig().getBoolean("settings.enchants.thorns")) {
                        return;
                    }
                    int level = armor.getEnchantmentLevel(Enchantment.THORNS);
                    enchant.thorns(horse, event.getDamager(), level);
                }
            }
        }
    }

    @EventHandler
    public void onItemEnchant(PrepareItemEnchantEvent event) {
        if (!event.getEnchanter().hasPermission("horsearmor.enchant")) {
            return;
        }
        if (!plugin.getConfig().getBoolean("settings.enchant")) {
            return;
        }
        List<String> armorList = plugin.getConfig().getStringList("settings.armor");
        if (!armorList.contains(event.getItem().getType().toString())) {
            return;
        }
        if (!event.getItem().getEnchantments().isEmpty()) {
            return;
        }
        event.getEnchanter().sendMessage("You need to add an enchantment in an anvil!");
    }

    @EventHandler
    public void onAnvilClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            if (event.getInventory() instanceof AnvilInventory) {
                if (event.getSlot() != 2) {
                    return;
                }
                Player player = (Player) event.getWhoClicked();
                if (!player.hasPermission("horsearmor.anvil")) {
                    return;
                }
                if (!plugin.getConfig().getBoolean("settings.anvil")) {
                    return;
                }
                Inventory inv = event.getInventory();

                ItemStack item = inv.getItem(0);
                ItemStack enchant = inv.getItem(1);
                ItemStack newItem = inv.getItem(2);
                if (item == null || enchant == null || newItem == null) {
                    return;
                }
                List<String> armorList = plugin.getConfig().getStringList("settings.armor");
                if (!armorList.contains(item.getType().toString())) {
                    return;
                }

                player.setItemOnCursor(newItem);
                inv.clear();
                player.playSound(inv.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
            }
        }
    }

    @EventHandler
    public void onAnvil(PrepareAnvilEvent event) {
        if (!event.getView().getPlayer().hasPermission("horsearmor.anvil")) {
            return;
        }
        if (!plugin.getConfig().getBoolean("settings.anvil")) {
            return;
        }
        Inventory anvil = event.getInventory();
        if (anvil == null) {
            return;
        }

        ItemStack armor = event.getInventory().getItem(0);;
        ItemStack second = event.getInventory().getItem(1);;
        if (armor == null || second == null) {
            return;
        }
        ItemStack result = armor.clone();
        List<String> armorList = plugin.getConfig().getStringList("settings.armor");
        if (!armorList.contains(armor.getType().toString())) {
            return;
        }
        Integer itemEXP = 0;
        for (Enchantment enchant : armor.getEnchantments().keySet()) {
            itemEXP += 2;
        }
        Integer bookEXP = 0;
        if (second.getType().equals(Material.ENCHANTED_BOOK)) {
            if (second.getItemMeta() instanceof EnchantmentStorageMeta) {
                EnchantmentStorageMeta meta = (EnchantmentStorageMeta) second.getItemMeta();
                Map<Enchantment, Integer> enchants = meta.getStoredEnchants();
                for (Map.Entry<Enchantment, Integer> entry : enchants.entrySet()) {
                    result.addUnsafeEnchantment(entry.getKey(), entry.getValue());
                    bookEXP += 2;
                }
            }
        }
        Integer repairCost = itemEXP + bookEXP;
        if (second.getType().equals(dur.getRepairItem(armor))) {
            int repairAmount = dur.getRepairAmount(armor) * second.getAmount();
            dur.changeDurability(result, (short) repairAmount);
        }
        plugin.getServer().getScheduler().runTask(plugin, () -> event.getInventory().setRepairCost(repairCost));
        event.setResult(result);
    }

    @EventHandler
    public void onItemCraft(PrepareItemCraftEvent event) {
        List<String> armorList = plugin.getConfig().getStringList("settings.armor");
        if (event.getRecipe() == null) {
            return;
        }
        ItemStack result = event.getRecipe().getResult();
        if (armorList.contains(result.getType().toString())) {
            dur.setDefaultDurability(result);
            event.getInventory().setResult(result);
        }
    }

    @EventHandler
    public void onItemClick(InventoryClickEvent event) {
        List<String> armorList = plugin.getConfig().getStringList("settings.armor");
        if (event.getCurrentItem() != null && armorList.contains(event.getCurrentItem().getType().toString())) {
            if (dur.getDurability(event.getCurrentItem()) == 0) {
                dur.setDefaultDurability(event.getCurrentItem());
            }
        }
        if (event.getCursor() != null && armorList.contains(event.getCursor().getType().toString())) {
            if (dur.getDurability(event.getCursor()) == 0) {
                dur.setDefaultDurability(event.getCursor());
            }
        }
    }
}
