package com.ranull.horsearmor;

import com.ranull.horsearmor.enchants.EnchantManager;
import com.ranull.horsearmor.events.Events;
import com.ranull.horsearmor.commands.HorseArmorCommand;
import com.ranull.horsearmor.durability.DurabilityManager;
import com.ranull.horsearmor.recipe.RecipeManager;
import org.bukkit.plugin.java.JavaPlugin;

public class HorseArmor extends JavaPlugin {
    RecipeManager recipe;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        DurabilityManager dur = new DurabilityManager();
        EnchantManager inv = new EnchantManager();
        recipe = new RecipeManager(this);
        recipe.addRecipes();

        this.getCommand("horsearmor").setExecutor(new HorseArmorCommand(this));
        this.getServer().getPluginManager().registerEvents(new Events(this, inv, dur), this);
    }

    @Override
    public void onDisable() {
        recipe.removeRecipes();
    }
}